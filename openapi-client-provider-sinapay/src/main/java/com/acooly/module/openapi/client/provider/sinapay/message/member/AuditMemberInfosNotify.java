/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月7日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayNotify;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhike
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.AUDIT_MEMBER_INFOS, type = ApiMessageType.Notify)
public class AuditMemberInfosNotify extends SinapayNotify {

    /**
     * 请求审核订单号
     *
     * 商户网站唯一订单号或者交易原始凭证号
     */
    @NotEmpty
    @Size(max = 32)
    @ApiItem(value = "audit_order_no")
    private String auditOrderNo;


    /**
     *
     */
    @NotEmpty
    @Size(max = 32)
    @ApiItem(value = "inner_order_no")
    private String innerOrderNo;


    /**
     * 内部交易凭证号
     *
     * 商户网站交易订单号，商户内部保证唯一
     */
    @NotEmpty
    @Size(max = 32)
    @ApiItem(value = "audit_status")
    private String auditStatus;


    /**
     * 审核结果
     *
     * SUCCESS 成功  FAILED 失败
     *
     */
    @Size(max = 32)
    @ApiItem(value = "audit_message")
    private String auditMessage;
}
