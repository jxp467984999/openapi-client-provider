package com.acooly.module.openapi.client.provider.sinapay.utils;

import com.acooly.module.openapi.client.api.exception.ApiClientException;

import javax.servlet.http.HttpServletRequest;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

/**
 * @author zhike 2018/3/5 19:51
 */
public class HttpServletRequestUtil implements Serializable{

    /**
     * 将收到的报文转化为map
     *
     * @param request
     * @return
     */
    public static Map<String, String> getNoticeDateMap(HttpServletRequest request) {
        try {
            request.setCharacterEncoding("utf-8");
            Map<String, String> params = new HashMap<String, String>();
            Map<String, String[]> requestParams = request.getParameterMap();
            for (Map.Entry<String, String[]> entry : requestParams.entrySet()) {
                String name = entry.getKey();
                String[] values = entry.getValue();
                String valueStr = "";
                if (values != null) {
                    for (int i = 0; i < values.length; i++) {
                        valueStr = (i == values.length - 1) ? valueStr + values[i]
                                : valueStr + values[i] + ",";
                    }

                    params.put(name, valueStr);
                }

            }
            return params;
        } catch (Exception e) {
            throw new ApiClientException("转化异步报文异常", e);
        }
    }
}
