package com.acooly.module.openapi.client.provider.sinapay.message.trade;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.annotation.SinaGateWay;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayResponse;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayGateWayType;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import com.acooly.module.openapi.client.provider.sinapay.message.trade.dto.DepositItem;
import com.google.common.collect.Lists;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * 3.11 充值查询
 *
 * @author xiaohong
 * @create 2018-07-18 9:55
 **/
@Getter
@Setter
@SinaGateWay(SinapayGateWayType.PAYMENT)
@SinapayApiMsg(service = SinapayServiceNameEnum.QUERY_HOSTING_DEPOSIT, type = ApiMessageType.Response)
public class QueryHostingDepositResponse extends SinapayResponse {
    /**
     * 充值明细列表
     */
    @ApiItem(value = "deposit_list")
    private List<DepositItem> depositList = Lists.newArrayList();

    /**
     * 页号
     */
    @ApiItem(value = "page_no")
    private int pageNo;

    /**
     * 每页大小
     */
    @ApiItem(value = "page_size")
    private int pageSize;

    /**
     * 总记录数
     */
    @ApiItem(value = "total_item")
    private int totalItem;
}
