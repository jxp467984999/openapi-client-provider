/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月16日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.trade.dto;

import com.acooly.core.utils.ToString;
import com.acooly.module.openapi.client.provider.sinapay.annotation.ApiDto;
import com.acooly.module.openapi.client.provider.sinapay.annotation.ItemOrder;
import com.acooly.module.openapi.client.provider.sinapay.message.Dtoable;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 
 * 批量代付通知明细
 * 
 * @author zhike
 */
@Getter
@Setter
@ApiDto(colSeparator = '~', rowSeparator = '$')
public class BatchPayItem implements Dtoable {

	/**
	 * 交易订单号
	 *
	 * 商户网站交易订单号，商户内部保证唯一
	 */
	@NotEmpty
	@Size(max = 32)
	@ItemOrder(0)
	private String outTradeNo;

	/** 摘要 */
	@NotEmpty
	@Size(max = 64)
	@ItemOrder(1)
	private String memo;

	@ItemOrder(2)
	private String tradeAmount;

	/**
	 * 交易状态
	 *
	 * 交易状态， 详见附录中的交易状态
	 */
	@NotEmpty
	@Size(max = 20)
	@ItemOrder(3)
	private String tradeStatus;

	@ItemOrder(4)
	private String createTime;

	@ItemOrder(5)
	private String modifyTime;

	@Override
	public String toString() {
		return ToString.toString(this);
	}

}
