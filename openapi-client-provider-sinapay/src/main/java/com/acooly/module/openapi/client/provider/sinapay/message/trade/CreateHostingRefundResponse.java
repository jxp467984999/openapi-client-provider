package com.acooly.module.openapi.client.provider.sinapay.message.trade;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.annotation.SinaGateWay;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayResponse;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayGateWayType;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 退款
 *
 * @author xiaohong
 * @create 2018-07-17 16:29
 **/
@Getter
@Setter
@SinaGateWay(SinapayGateWayType.PAYMENT)
@SinapayApiMsg(service = SinapayServiceNameEnum.CREATE_HOSTING_REFUND, type = ApiMessageType.Response)
public class CreateHostingRefundResponse extends SinapayResponse {

    /**
     * 交易订单号
     */
    @NotEmpty
    @Size(max = 32)
    @ApiItem(value = "out_trade_no")
    private String outTradeNo;

    /**
     * 退款状态
     */
    @Size(max = 16)
    @ApiItem(value = "refund_status")
    private String refundStatus;
}
