package com.acooly.module.openapi.client.provider.sinapay.message.trade.dto;

import com.acooly.module.openapi.client.provider.sinapay.annotation.ApiDto;
import com.acooly.module.openapi.client.provider.sinapay.annotation.ItemOrder;
import com.acooly.module.openapi.client.provider.sinapay.message.Dtoable;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * 充值条目
 *
 * @author xiaohong
 * @create 2018-07-18 10:21
 **/
@Getter
@Setter
@ApiDto
public class DepositItem implements Dtoable {
    /**
     * 充值订单号
     *
     * 外部交易号
     */
    @NotEmpty
    @Size(max = 64)
    @ItemOrder(0)
    private String outTradeNo;

    /**
     * 金额
     *
     * 单位为：RMB Yuan。精确到小数点后两位。
     */
    @NotEmpty
    @ItemOrder(1)
    private String tradeAmount;

    /**
     * 状态
     *
     * 充值状态， 详见附录中的充值状态
     */
    @NotEmpty
    @Size(max = 16)
    @ItemOrder(2)
    private String tradeStatus;

    /**
     * 创建时间
     *
     * 数字串，一共14位 格式为：年[4位]月[2位]日[2位]时[2位]分[2位]秒[2位]
     */
    @NotEmpty
    @Size(max = 14)
    @ItemOrder(3)
    private String createTime;

    /**
     * 最后修改时间
     *
     * 数字串，一共14位 格式为：年[4位]月[2位]日[2位]时[2位]分[2位]秒[2位]
     */
    @NotEmpty
    @Size(max = 14)
    @ItemOrder(4)
    private String modifyTime;
}
