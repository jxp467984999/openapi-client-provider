/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年5月6日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.trade;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.annotation.SinaGateWay;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayGateWayType;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author zhike
 */
@Getter
@Setter
@SinaGateWay(SinapayGateWayType.PAYMENT)
@SinapayApiMsg(service = SinapayServiceNameEnum.QUERY_PAY_RESULT, type = ApiMessageType.Request)
public class QueryPayResultRequest extends SinapayRequest {

	/**
	 * 支付请求号
	 *
	 * 商户网站交易订单号，商户内部保证唯一
	 */
	@NotEmpty
	@Size(max = 32)
	@ApiItem(value = "out_pay_no")
	private String outPayNo;

	public QueryPayResultRequest() {
		super();
	}

	/**
	 * @param outPayNo
	 */
	public QueryPayResultRequest(String outPayNo) {
		super();
		this.outPayNo = outPayNo;
	}
}
