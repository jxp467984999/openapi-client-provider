package com.acooly.module.openapi.client.provider.sinapay;

import com.acooly.module.openapi.client.api.notify.ApiServiceClientServlet;
import com.acooly.module.openapi.client.api.transport.HttpTransport;
import com.acooly.module.openapi.client.api.transport.Transport;
import com.acooly.module.openapi.client.provider.sinapay.file.SinapaySftpFileHandler;
import com.acooly.module.openapi.client.provider.sinapay.notify.SinapayApiServiceClientServlet;
import com.google.common.collect.Lists;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import java.util.List;


@EnableConfigurationProperties({OpenAPIClientSinapayProperties.class})
@ConditionalOnProperty(value = OpenAPIClientSinapayProperties.PREFIX + ".enable", matchIfMissing = true)
@ComponentScan
@ComponentScan("com.acooly.module.openapi.client.file")
public class OpenAPIClientSinapayConfigration {

    @Autowired
    private OpenAPIClientSinapayProperties openAPIClientSinapayProperties;

    @Bean("sinapayHttpTransport")
    public Transport sinapayHttpTransport() {
        HttpTransport httpTransport = new HttpTransport();
        httpTransport.setConnTimeout(String.valueOf(openAPIClientSinapayProperties.getConnTimeout()));
        httpTransport.setReadTimeout(String.valueOf(openAPIClientSinapayProperties.getReadTimeout()));
        httpTransport.setContentType("application/x-www-form-urlencoded");
        httpTransport.setCharset("UTF-8");
        return httpTransport;
    }

    /**
     * 接受异步通知的filter
     *
     * @return
     */
    @Bean
    public ServletRegistrationBean sinapayClientServlet() {
        ServletRegistrationBean bean = new ServletRegistrationBean();
        SinapayApiServiceClientServlet apiServiceClientServlet = new SinapayApiServiceClientServlet();
        bean.setServlet(apiServiceClientServlet);
        bean.addInitParameter(ApiServiceClientServlet.NOTIFY_DISPATCHER_BEAN_NAME_KEY, "sinapayNotifyHandlerDispatcher");
        List<String> urlMappings = Lists.newArrayList();       //网关异步通知地址
        urlMappings.add(SinapayConstants.getCanonicalUrl(openAPIClientSinapayProperties.getNotifyUrl(), "/*"));//访问，可以添加多个
        bean.setUrlMappings(urlMappings);
        bean.setLoadOnStartup(3);
        return bean;
    }


    @Bean("sinapaySftpFileHandler")
    public SinapaySftpFileHandler sinapaySftpFileHandler() {
        SinapaySftpFileHandler sinapaySftpFileHandler = new SinapaySftpFileHandler();
        sinapaySftpFileHandler.setHost(openAPIClientSinapayProperties.getCheckfile().getHost());
        sinapaySftpFileHandler.setPort(openAPIClientSinapayProperties.getCheckfile().getPort());
        sinapaySftpFileHandler.setUsername(openAPIClientSinapayProperties.getCheckfile().getUsername());
        sinapaySftpFileHandler.setPassword(openAPIClientSinapayProperties.getCheckfile().getPassword());
        sinapaySftpFileHandler.setServerRoot(openAPIClientSinapayProperties.getCheckfile().getServerRoot());
        sinapaySftpFileHandler.setLocalRoot(openAPIClientSinapayProperties.getCheckfile().getLocalRoot());
        return sinapaySftpFileHandler;
    }


}
