/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年4月29日
 *
 */
package com.acooly.module.openapi.client.provider.sinapay.message.member;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayApiMsg;
import com.acooly.module.openapi.client.provider.sinapay.domain.SinapayRequest;
import com.acooly.module.openapi.client.provider.sinapay.enums.SinapayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

/**
 * @author liubin
 */
@Getter
@Setter
@SinapayApiMsg(service = SinapayServiceNameEnum.QUERY_IS_SET_PAY_PASSWORD, type = ApiMessageType.Request)
public class QuerySetPayPasswordRequest extends SinapayRequest {

	/** 用户标识信息 */
	@Size(max = 50)
	@NotEmpty
	@ApiItem("identity_id")
	private String identityId;

	/** 用户标识类型 */
	@Size(max = 16)
	@NotEmpty
	@ApiItem("identity_type")
	private String identityType = "UID";
}
