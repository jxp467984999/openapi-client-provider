/**
 * create by zhangpu
 * date:2015年3月12日
 */
package com.acooly.module.openapi.client.provider.cmb.marshall;

import com.acooly.core.utils.Reflections;
import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.exception.ApiClientException;
import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.api.message.MessageFactory;
import com.acooly.module.openapi.client.provider.cmb.CmbConstants;
import com.acooly.module.openapi.client.provider.cmb.domain.CmbNotify;
import com.acooly.module.openapi.client.provider.cmb.enums.CmbServiceEnum;
import com.acooly.module.openapi.client.provider.cmb.partner.CmbPartnerIdLoadManager;
import com.acooly.module.openapi.client.provider.cmb.support.CmbRespCodes;
import com.acooly.module.openapi.client.provider.cmb.utils.SignUtils;
import com.acooly.module.safety.Safes;
import com.acooly.module.safety.key.KeyLoadManager;
import com.acooly.module.safety.signature.SignTypeEnum;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Field;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;

/**
 * @author zhangpu
 */
@Service
public class CmbNotifyUnmarshall extends com.acooly.module.openapi.client.provider.cmb.marshall.CmbMarshallSupport
        implements ApiUnmarshal<CmbNotify, Map<String, String>> {
    private static final Logger logger = LoggerFactory.getLogger(CmbNotifyUnmarshall.class);
    @Resource(name = "weBankMessageFactory")
    private MessageFactory messageFactory;

    @Resource(name = "weBankPartnerIdLoadManager")
    private CmbPartnerIdLoadManager partnerIdLoadManager;

    @Autowired
    private KeyLoadManager keyStoreLoadManager;

    @SuppressWarnings("unchecked")
    @Override
    public CmbNotify unmarshal(Map<String, String> message, String serviceName) {
        try {
            logger.info("异步通知{}", message);
            String signature = message.get(CmbConstants.SIGN);
            message.remove(CmbConstants.SIGN);
            String plain = SignUtils.getSignContent(message);
            //验签
            Safes.getSigner(SignTypeEnum.Rsa.name ()).verify(plain, getKeyPair(), signature);
            return doUnmarshall(message, serviceName);
        } catch (Exception e) {
            throw new ApiClientException("异步通知 解析失败:" + e.getMessage());
        }

    }

    protected CmbNotify doUnmarshall(Map<String, String> message, String serviceName) {
        CmbNotify notify = (CmbNotify) messageFactory.getNotify(serviceName);
        Set<Field> fields = Reflections.getFields(notify.getClass());
        String key = null;
        for (Field field : fields) {
            ApiItem apiItem = field.getAnnotation(ApiItem.class);
            if (apiItem != null && Strings.isNotBlank(apiItem.value())) {
                key = apiItem.value();
            }else {
                key = field.getName();
            }
            Reflections.invokeSetter(notify, field.getName(), message.get(key));
        }
        notify.setBizType(CmbServiceEnum.find(serviceName).getCode());
        if (Strings.isNotBlank(notify.getCode())) {
            notify.setMessage(CmbRespCodes.getMessage(notify.getCode()));
        }
        return notify;
    }

    public void setMessageFactory(MessageFactory messageFactory) {
        this.messageFactory = messageFactory;
    }

}
