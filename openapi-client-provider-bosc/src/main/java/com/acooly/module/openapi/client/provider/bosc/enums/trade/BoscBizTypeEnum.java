/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-24 19:29 创建
 */
package com.acooly.module.openapi.client.provider.bosc.enums.trade;

import com.acooly.core.utils.enums.Messageable;
import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * BOSC业务类型
 *
 * @author zhangpu 2017-09-24 19:29
 */
public enum BoscBizTypeEnum implements Messageable {
    
    TENDER ("TENDER", "投标"),
    
    REPAYMENT("REPAYMENT", "还款"),
    
    CREDIT_ASSIGNMENT("CREDIT_ASSIGNMENT", "债权认购"),
    
    COMPENSATORY("COMPENSATORY", "代偿"),
    
    COMPENSATORY_REPAYMENT("COMPENSATORY_REPAYMENT","还代偿款"),
    
    MARKETING("MARKETING","营销红包"),
    
    INTEREST("INTEREST","派息"),
    
    ALTERNATIVE_RECHARGE("ALTERNATIVE_RECHARGE","代充值"),
    
    INTEREST_REPAYMENT("INTEREST_REPAYMENT","还派息款"),
    
    COMMISSION("COMMISSION","佣金"),
    
    PROFIT("PROFIT","关联分润"),
    
    DEDUCT("DEDUCT","平台服务费"),
    
    FUNDS_TRANSFER("FUNDS_TRANSFER","平台资金划拨"),
    ;

    private final String code;
    private final String message;

    private BoscBizTypeEnum (String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = Maps.newLinkedHashMap();
        for (BoscBizTypeEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static BoscBizTypeEnum find(String code) {
        for (BoscBizTypeEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        throw new IllegalArgumentException("BoscServiceNameEnum not legal:" + code);
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<BoscBizTypeEnum> getAll() {
        List<BoscBizTypeEnum> list = new ArrayList<BoscBizTypeEnum>();
        for (BoscBizTypeEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (BoscBizTypeEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }

}
