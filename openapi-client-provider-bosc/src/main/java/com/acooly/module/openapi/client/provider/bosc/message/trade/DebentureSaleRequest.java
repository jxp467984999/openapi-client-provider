package com.acooly.module.openapi.client.provider.bosc.message.trade;

import com.acooly.core.utils.Money;
import com.acooly.core.utils.validate.jsr303.MoneyConstraint;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

@ApiMsgInfo(service = BoscServiceNameEnum.DEBENTURE_SALE, type = ApiMessageType.Request)
public class DebentureSaleRequest extends BoscRequest {
	/**
	 * 请求流水号
	 */
	@NotEmpty
	@Size(max = 50)
	private String requestNo;
	/**
	 * 债权出让平台用户编号
	 */
	@NotEmpty
	@Size(max = 50)
	private String platformUserNo;
	/**
	 * 标的号
	 */
	@NotEmpty
	@Size(max = 50)
	private String projectNo;
	/**
	 * 出让份额
	 */
	@MoneyConstraint(min = 1)
	private Money saleShare;
	
	public DebentureSaleRequest () {
		setService (BoscServiceNameEnum.DEBENTURE_SALE.code ());
	}
	
	public DebentureSaleRequest (String requestNo, String platformUserNo, String projectNo,
	                             Money saleShare) {
		this();
		this.requestNo = requestNo;
		this.platformUserNo = platformUserNo;
		this.projectNo = projectNo;
		this.saleShare = saleShare;
	}
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
	
	public String getPlatformUserNo () {
		return platformUserNo;
	}
	
	public void setPlatformUserNo (String platformUserNo) {
		this.platformUserNo = platformUserNo;
	}
	
	public String getProjectNo () {
		return projectNo;
	}
	
	public void setProjectNo (String projectNo) {
		this.projectNo = projectNo;
	}
	
	public Money getSaleShare () {
		return saleShare;
	}
	
	public void setSaleShare (Money saleShare) {
		this.saleShare = saleShare;
	}
}