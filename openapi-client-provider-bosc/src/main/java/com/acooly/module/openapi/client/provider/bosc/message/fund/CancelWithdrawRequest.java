package com.acooly.module.openapi.client.provider.bosc.message.fund;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscRequest;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

@ApiMsgInfo(service = BoscServiceNameEnum.CANCEL_WITHDRAW, type = ApiMessageType.Request)
public class CancelWithdrawRequest extends BoscRequest {
	/**
	 * 请求流水号
	 */
	@NotEmpty
	@Size(max = 50)
	private String requestNo;
	/**
	 * 待确认提现请求流水号
	 */
	@NotEmpty
	@Size(max = 50)
	private String preTransactionNo;
	
	public CancelWithdrawRequest () {
		setService (BoscServiceNameEnum.CANCEL_WITHDRAW.code ());
	}
	
	public CancelWithdrawRequest (String requestNo, String preTransactionNo) {
		this();
		this.requestNo = requestNo;
		this.preTransactionNo = preTransactionNo;
	}
	
	public String getPreTransactionNo () {
		return preTransactionNo;
	}
	
	public void setPreTransactionNo (String preTransactionNo) {
		this.preTransactionNo = preTransactionNo;
	}
	
	public String getRequestNo () {
		return requestNo;
	}
	
	public void setRequestNo (String requestNo) {
		this.requestNo = requestNo;
	}
}