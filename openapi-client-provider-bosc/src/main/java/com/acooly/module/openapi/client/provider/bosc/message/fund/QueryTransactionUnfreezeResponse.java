package com.acooly.module.openapi.client.provider.bosc.message.fund;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscResponse;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.message.fund.info.UnfreezeDetailInfo;

import java.util.List;

@ApiMsgInfo(service = BoscServiceNameEnum.QUERY_TRANSACTION_UNFREEZE ,type = ApiMessageType.Response)
public class QueryTransactionUnfreezeResponse extends BoscResponse {
	
	private List<UnfreezeDetailInfo> records;
	
	public List<UnfreezeDetailInfo> getRecords () {
		return records;
	}
	
	public void setRecords (
			List<UnfreezeDetailInfo> records) {
		this.records = records;
	}
}