package com.acooly.module.openapi.client.provider.bosc.message.fund;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.bosc.domain.ApiMsgInfo;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscResponse;
import com.acooly.module.openapi.client.provider.bosc.enums.BoscServiceNameEnum;
import com.acooly.module.openapi.client.provider.bosc.message.fund.info.WithdrawDetailInfo;

import java.util.List;

@ApiMsgInfo(service = BoscServiceNameEnum.QUERY_TRANSACTION_WITHDRAW ,type = ApiMessageType.Response)
public class QueryTransactionWithdrawResponse extends BoscResponse {

	private  List<WithdrawDetailInfo> records;
	
	public List<WithdrawDetailInfo> getRecords () {
		return records;
	}
	
	public void setRecords (
			List<WithdrawDetailInfo> records) {
		this.records = records;
	}
}