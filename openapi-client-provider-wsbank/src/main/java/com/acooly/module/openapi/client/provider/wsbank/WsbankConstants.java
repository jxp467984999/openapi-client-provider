/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhike@acooly.cn 2017-09-17 17:49 创建
 */
package com.acooly.module.openapi.client.provider.wsbank;

/**
 * @author zhike 2017-09-17 17:49
 */
public class WsbankConstants {
    /**
     * 签名实现KEY和秘钥
     */
    public static final String PROVIDER_NAME = "wsbankProvider";
    public static final String PROVIDER_DEF_PRINCIPAL = "principal";

    public static final String SIGN = "sign";
    public static final String NOTIFY_DATA_KEY = "notifyData";
    public static final String NOTIFY_RESPONSE_DATA_KEY = "notifyResponseData";
    
    //支付宝APP/WAP支付
    public static final String ALI_HTML_METHOD = "get";
    public static final String ALI_APP_ID = "app_id";
    public static final String ALI_METHOD = "method";
    public static final String ALI_FORMAT = "format";
    public static final String ALI_CHARSET = "charset";
    public static final String ALI_SIGN_TYPE = "sign_type";
    public static final String ALI_SIGN = "sign";
    public static final String ALI_TIMESTAMP = "timestamp";
    public static final String ALI_VERSION = "version";
    public static final String ALI_RETURN_URL = "return_url";
    public static final String ALI_NOTIFY_URL = "notify_url";
    public static final String ALI_BIZ_CONTENT = "biz_content";
}
