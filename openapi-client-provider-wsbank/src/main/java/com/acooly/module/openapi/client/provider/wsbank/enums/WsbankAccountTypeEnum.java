package com.acooly.module.openapi.client.provider.wsbank.enums;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 商户类型枚举
 * @author sunjx
 *
 */
public enum WsbankAccountTypeEnum implements Messageable {

	PRIVATE_ACCOUNT("01", "对私账户"),
	PUBLIC_ACCOUNT("02", "对公账户");

    private final String code;
    private final String message;

    WsbankAccountTypeEnum(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        for (WsbankAccountTypeEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static WsbankAccountTypeEnum find(String code) {
        for (WsbankAccountTypeEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<WsbankAccountTypeEnum> getAll() {
        List<WsbankAccountTypeEnum> list = new ArrayList<WsbankAccountTypeEnum>();
        for (WsbankAccountTypeEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (WsbankAccountTypeEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }
}
