package com.acooly.module.openapi.client.provider.wsbank.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

@Getter
@Setter
@XStreamAlias("body")
public class WsbankBkCloudFundsVostroQueryOrderRequestBody implements Serializable {

	private static final long serialVersionUID = -4583290771190715615L;

	/**
	 * 合作方机构号
	 */
	@Size(max = 32)
	@XStreamAlias("IsvOrgId")
	@NotBlank
	private String isvOrgId;
	
	/**
	 * 商户号
	 */
	@Size(max = 32)
	@XStreamAlias("MerchantId")
	@NotBlank
	private String merchantId;
	
	/**
	 * 原外部订单请求流水号
	 */
	@Size(max = 64)
	@XStreamAlias("OutTradeNo")
	@NotBlank
	private String outTradeNo;
	
	/**
	 * 原网商订单号
	 */
	@Size(max = 64)
	@XStreamAlias("OrderNo")
	@NotBlank
	private String orderNo;
}
