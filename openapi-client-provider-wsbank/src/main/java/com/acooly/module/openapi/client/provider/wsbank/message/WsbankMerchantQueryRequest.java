package com.acooly.module.openapi.client.provider.wsbank.message;

import com.acooly.core.utils.validate.Validators;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankApiMsgInfo;
import com.acooly.module.openapi.client.provider.wsbank.domain.WsbankRequest;
import com.acooly.module.openapi.client.provider.wsbank.enums.WsbankServiceEnum;
import com.acooly.module.openapi.client.provider.wsbank.message.dto.WsbankMerchantQueryRequestInfo;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/**
 * @author weili 2018/5/22 15:32
 */
@Getter
@Setter
@XStreamAlias("document")
@WsbankApiMsgInfo(service = WsbankServiceEnum.TRADE_ORDER_QUERY,type = ApiMessageType.Request)
public class WsbankMerchantQueryRequest extends WsbankRequest {

    @XStreamAlias("request")
    private WsbankMerchantQueryRequestInfo requestInfo;

    @Override
    public void doCheck() {
        Validators.assertJSR303(requestInfo);
        Validators.assertJSR303(requestInfo.getHeadRequest());
        Validators.assertJSR303(requestInfo.getRequestBody());
    }
}
