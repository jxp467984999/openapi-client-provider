/**
 * create by zhangpu
 * date:2015年3月12日
 */
package com.acooly.module.openapi.client.provider.newyl.marshall;

import com.acooly.module.openapi.client.api.marshal.ApiUnmarshal;
import com.acooly.module.openapi.client.api.message.MessageFactory;
import com.acooly.module.openapi.client.provider.newyl.NewYlProperties;
import com.acooly.module.openapi.client.provider.newyl.domain.NewYlResponse;
import com.acooly.module.openapi.client.provider.newyl.exception.ApiClientYlProcessingException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import lombok.extern.slf4j.Slf4j;


/**
 * @author zhangpu
 */
@Slf4j
@Service
public class NewYlResponseUnmarshall extends NewYlMarshallSupport implements ApiUnmarshal<NewYlResponse, String> {

    private static final Logger logger = LoggerFactory.getLogger(NewYlResponseUnmarshall.class);

    @Resource(name = "newYlMessageFactory")
    private MessageFactory messageFactory;

    @Autowired
    protected NewYlProperties newYlProperties;

    @SuppressWarnings("unchecked")
    @Override
    public NewYlResponse unmarshal(String message, String serviceName) {
        try {
            logger.info("响应报文:{}", message);
            NewYlResponse ylResponse = (NewYlResponse) messageFactory.getResponse (serviceName);
            doVerifySign(message,newYlProperties.getPartnerId());
            ylResponse = (NewYlResponse)ylResponse.str2Obj(message,ylResponse);
            return ylResponse;
        } catch (Exception e) {
            throw new ApiClientYlProcessingException("解析响应报文错误:" + e.getMessage());
        }
    }
}
