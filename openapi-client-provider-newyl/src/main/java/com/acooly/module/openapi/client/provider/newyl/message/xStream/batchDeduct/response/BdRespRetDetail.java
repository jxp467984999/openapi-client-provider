package com.acooly.module.openapi.client.provider.newyl.message.xStream.batchDeduct.response;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Data;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

/**
 * @author fufeng 2018/1/26 15:30.
 */
@Data
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("RET_DETAIL")
public class BdRespRetDetail {
    /**
     *记录序号
     */
    @XStreamAlias("SN")
    private String sn;
    /**
     *返回码
     */
    @XStreamAlias("RET_CODE")
    private String retCode;
    /**
     *错误文本
     */
    @XStreamAlias("ERR_MSG")
    private String errMsg;
    /**
     *DATE_SETTLMT
     */
    @XStreamAlias("DATE_SETTLMT")
    private String dateSetTlmt;

}
