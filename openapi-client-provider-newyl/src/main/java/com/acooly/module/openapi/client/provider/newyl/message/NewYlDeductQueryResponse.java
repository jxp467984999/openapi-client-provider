package com.acooly.module.openapi.client.provider.newyl.message;


import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.newyl.domain.NewYlApiMsgInfo;
import com.acooly.module.openapi.client.provider.newyl.domain.NewYlResponse;
import com.acooly.module.openapi.client.provider.newyl.enums.NewYlServiceEnum;
import com.acooly.module.openapi.client.provider.newyl.message.xStream.common.RespInfo;
import com.acooly.module.openapi.client.provider.newyl.message.xStream.deductQuery.response.QdRespBody;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@XmlAccessorType(XmlAccessType.FIELD)
@XStreamAlias("GZZF")
@NewYlApiMsgInfo(service = NewYlServiceEnum.NEW_YL_DEDUCT_QUERY, type = ApiMessageType.Response)
public class NewYlDeductQueryResponse extends NewYlResponse {

    @XStreamAlias("INFO")
    private RespInfo respInfo;
    @XStreamAlias("BODY")
    private QdRespBody qdRespBody;

}
