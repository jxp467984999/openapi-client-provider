package com.acooly.module.openapi.client.provider.yinsheng.message;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengApiMsgInfo;
import com.acooly.module.openapi.client.provider.yinsheng.domain.YinShengRequest;
import com.acooly.module.openapi.client.provider.yinsheng.enums.YinShengServiceEnum;
import com.acooly.module.openapi.client.provider.yinsheng.message.dto.YinShengFastPayInfo;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

@Getter
@Setter
@YinShengApiMsgInfo(service = YinShengServiceEnum.FAST_PAY, type = ApiMessageType.Request)
public class YinShengFastPayRequest extends YinShengRequest {

    /**
     * 通知地址
     */
    @NotBlank
    private String notify_url;
    /**
     * 快捷申请业务信息
     */
    @ApiItem(value = "biz_content")
    private YinShengFastPayInfo yinShengFastPayInfo;
}
