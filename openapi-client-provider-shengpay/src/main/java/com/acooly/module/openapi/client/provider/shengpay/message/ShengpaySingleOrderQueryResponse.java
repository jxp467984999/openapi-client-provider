package com.acooly.module.openapi.client.provider.shengpay.message;

import com.acooly.core.utils.Dates;
import com.acooly.core.utils.Strings;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.shengpay.ShengpayConstants;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayAlias;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayApiMsg;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayResponse;
import com.acooly.module.openapi.client.provider.shengpay.enums.ShengpayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import java.util.Date;

/**
 * @author zhike 2018/5/21 15:09
 */
@Getter
@Setter
@ShengpayApiMsg(service = ShengpayServiceNameEnum.ORDER_QUERY,type = ApiMessageType.Response)
public class ShengpaySingleOrderQueryResponse extends ShengpayResponse {
    /**
     * 本名称,默认属性值为:REP_B2CPAYMENT
     */
    /**
     * 版本名称
     * 版本名称,默认属性值为:QUERY_ORDER_REQUEST
     */
    @NotBlank
    @ShengpayAlias("ServiceCode")
    private String serviceCode;

    /**
     * 版本号,默认属性值为: V4.4.1.1.1
     */
    @ShengpayAlias("Version")
    private String version;

    /**
     * 字符集,支持GBK、UTF-8、GB2312,默认属性值为:UTF-8
     */
    @ShengpayAlias("Charset")
    private String charset = ShengpayConstants.SHENGPAY_CHARSET;

    /**
     * 报文发起方唯一消息标识
     */
    @ShengpayAlias("TraceNo")
    private String traceNo;

    /**
     * 由盛付通提供,默认为:商户号(由盛付通提供的6位正整数)
     */
    @ShengpayAlias("SenderId")
    private String senderId;

    /** 商户网站提交查询请求,为14位正整数数字,格式为:yyyyMMddHHmmss,如:20110707112233 */
    @ShengpayAlias("SendTime")
    private String sendTime = Dates.format(new Date(),"yyyyMMddHHmmss");

    /**
     * 产品订单号
     */
    @ShengpayAlias("OrderNo")
    private String orderNo;

    /**
     * 订单金额
     */
    @ShengpayAlias("OrderAmount")
    private String orderAmount;

    /**
     * 产品订单号
     */
    @ShengpayAlias("TransNo")
    private String transNo;

    /**
     * 订单金额
     */
    @ShengpayAlias("TransAmount")
    private String transAmount;

    /**
     * TransStatus
     */
    @ShengpayAlias("TransStatus")
    private String transStatus;

    /**
     * 时间，为14位正整数数字,格式为:yyyyMMddHHmmss,如:20110707112233
     */
    @ShengpayAlias("TransTime")
    private String transTime;

    /**
     * 英文或中文字符串
     */
    @ShengpayAlias("Ext1")
    private String ext1;

    /**
     * 签名类型,支持类型：RSA
     */
    @ShengpayAlias("SignType")
    private String signType;

    /**
     * 签名结果
     */
    @ShengpayAlias("SignMsg")
    private String signMsg;

    /**
     * 错误码
     */
    private String errorCode;

    /**
     * 错误信息
     */
    private String errorMsg;

    @Override
    public String getSignStr() {
        StringBuilder signStr = new StringBuilder();
        if (Strings.isNotBlank(getServiceCode())) {
            signStr.append(getServiceCode()+"|");
        }
        if (Strings.isNotBlank(getVersion())) {
            signStr.append(getVersion()+"|");
        }
        if (Strings.isNotBlank(getCharset())) {
            signStr.append(getCharset()+"|");
        }
        if (Strings.isNotBlank(getTraceNo())) {
            signStr.append(getTraceNo()+"|");
        }
        if (Strings.isNotBlank(getSenderId())) {
            signStr.append(getSenderId()+"|");
        }
        if (Strings.isNotBlank(getSendTime())) {
            signStr.append(getSendTime()+"|");
        }
        if (Strings.isNotBlank(getOrderNo())) {
            signStr.append(getOrderNo()+"|");
        }
        if (Strings.isNotBlank(getOrderAmount())) {
            signStr.append(getOrderAmount()+"|");
        }
        if (Strings.isNotBlank(getTraceNo())) {
            signStr.append(getTraceNo()+"|");
        }
        if (Strings.isNotBlank(getTransAmount())) {
            signStr.append(getTransAmount()+"|");
        }
        if (Strings.isNotBlank(getTransStatus())) {
            signStr.append(getTransStatus()+"|");
        }
        if (Strings.isNotBlank(getTransTime())) {
            signStr.append(getTransTime() + "|");
        }
        if (Strings.isNotBlank(getExt1())) {
            signStr.append(getExt1()+"|");
        }
        if (Strings.isNotBlank(getSignType())) {
            signStr.append(getSignType()+"|");
        }
        return signStr.toString();
    }
}
