package com.acooly.module.openapi.client.provider.shengpay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayApiMsg;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayRequest;
import com.acooly.module.openapi.client.provider.shengpay.enums.ShengpayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

/**
 * @author zhike 2018/5/17 19:05
 */
@Getter
@Setter
@ShengpayApiMsg(service = ShengpayServiceNameEnum.UNSIGN,type = ApiMessageType.Request)
public class ShengpayUnsignRequest extends ShengpayRequest {

    /**
     * 签约协议号
     */
    @NotBlank
    private String agreementNo;

    /**
     * 签约主体（商户的会员号，即outMemberId）
     */
    @NotBlank
    private String principalId;
}
