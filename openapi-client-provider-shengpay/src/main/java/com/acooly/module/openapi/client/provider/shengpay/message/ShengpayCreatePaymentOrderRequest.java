package com.acooly.module.openapi.client.provider.shengpay.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayApiMsg;
import com.acooly.module.openapi.client.provider.shengpay.domain.ShengpayRequest;
import com.acooly.module.openapi.client.provider.shengpay.enums.ShengpayServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/5/17 18:57
 */
@Getter
@Setter
@ShengpayApiMsg(service = ShengpayServiceNameEnum.CREATE_PAYMENT_ORDER,type = ApiMessageType.Request)
public class ShengpayCreatePaymentOrderRequest extends ShengpayRequest {

    /**
     * 商户系统内的唯一订单号
     */
    @NotBlank
    @Size(max = 32)
    private String merchantOrderNo;

    /**
     * 商品名称
     */
    @NotBlank
    private String productName;

    /**
     * 商品描述
     */
    private String productDesc;

    /**
     * 货币类型
     */
    private String currency = "CNY";

    /**
     * 该笔订单的交易金额，单位默认为RMB-元，精确到小数点后两位，如：23.42
     */
    @NotBlank
    private String amount;

    /**
     * 用户IP
     */
    @NotBlank
    private String userIp;
}
