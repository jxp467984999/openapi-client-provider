package com.acooly.module.openapi.client.provider.yibao.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoAlias;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoApiMsg;
import com.acooly.module.openapi.client.provider.yibao.domain.YibaoResponse;
import com.acooly.module.openapi.client.provider.yibao.enums.YibaoServiceNameEnum;
import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/6/26 14:31
 */
@Getter
@Setter
@YibaoApiMsg(service = YibaoServiceNameEnum.YIBAO_BINDPAY_RESENDSMS,type = ApiMessageType.Response)
public class YibaoSmsBindPayResendSmsResponse extends YibaoResponse {

    /**
     * 银行流水号
     */
    @YibaoAlias(value = "yborderid")
    private String bankOrderNo;

    /**
     * 订单状态
     * TO_VALIDATE：待短验确认
     * PAY_FAIL：支付失败
     * TIME_OUT：超时失败
     * FAIL：系统异常
     * （FAIL 是非终态是异常状态，出现此状态建议查
     * 询）
     */
    @YibaoAlias(value = "status")
    private String status;

    /**
     * 短验码
     * 商户发短验时返回的易宝生成的短
     * 验码（易宝发短验时此字段为空）
     */
    @YibaoAlias(value = "smscode")
    private String smsCode;

    /**
     * 实际短验发送方
     * CUSTOMER：商户发送
     * YEEPAY：易宝发送
     * BANK：银行发送
     */
    @YibaoAlias(value = "codesender")
    private String codeSender;
}
