/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-24 19:29 创建
 */
package com.acooly.module.openapi.client.provider.cj.enums;

import com.acooly.core.utils.enums.Messageable;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 * 服务名称枚举
 *
 * @author
 */
public enum CjServiceEnum implements Messageable {

    CJ_REAL_DEDUCT("cjRealDeduct","G10016", "畅捷单笔代扣"),
    CJ_BATCH_DEDUCT("cjBatchDeduct","G10003", "畅捷批量代扣"),
    CJ_REAL_DEDUCT_QUERY("cjRealDeductQuery","G20001", "畅捷实时代扣结果查询"),
    CJ_BATCH_DEDUCT_QUERY("cjBatchDeductQuery","G20002", "畅捷批量代扣结果查询"),
    ;
    private final String code;
    private final String key;
    private final String message;

    private CjServiceEnum(String code, String key, String message) {
        this.code = code;
        this.message = message;
        this.key = key;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public String code() {
        return code;
    }

    public String message() {
        return message;
    }

    public String getKey() {
        return key;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = new LinkedHashMap<String, String>();
        for (CjServiceEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static CjServiceEnum find(String code) {
        for (CjServiceEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param key 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 key 没有对应的 Status 。
     */
    public static CjServiceEnum findByKey(String key) {
        for (CjServiceEnum status : values()) {
            if (status.getKey().equals(key)) {
                return status;
            }
        }
        return null;
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<CjServiceEnum> getAll() {
        List<CjServiceEnum> list = new ArrayList<CjServiceEnum>();
        for (CjServiceEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (CjServiceEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }


}
