package com.acooly.module.openapi.client.provider.fuyou.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouRequest;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/3/19 15:04
 */
@Getter
@Setter
@FuyouApiMsgInfo(service = FuyouServiceEnum.FUYOU_WITHDRAW_QUERY,type = ApiMessageType.Request)
@XStreamAlias("qrytransreq")
public class FuyouWithdrawQueryRequest extends FuyouRequest{

    /**
     * 版本号
     * 1.0
     */
    @XStreamAlias("ver")
    @NotBlank
    @Size(max = 4)
    private String ver = "1.00";

    /**
     * 业务代码
     * AC01
     */
    @XStreamAlias("busicd")
    @NotBlank
    @Size(max = 4)
    private String busiCd = "AP01";

    /**
     * 原请求流水
     * 商户通过接口请求代收付交易时的流水号
     */
    @XStreamAlias("orderno")
    @Size(max = 30)
    private String orderNo;

    /**
     * 开始日期
     * yyyyMMdd
     */
    @XStreamAlias("startdt")
    @NotBlank
    @Size(max = 8)
    private String startDt;

    /**
     * 结束日期
     * yyyyMMdd 开始和结束时间间隔不能超过 15 天
     */
    @XStreamAlias("enddt")
    @NotBlank
    @Size(max = 8)
    private String endDt;

    /**
     * 交易状态
     */
    @XStreamAlias("transst")
    @NotBlank
    @Size(max = 1)
    private String transSt;

}
