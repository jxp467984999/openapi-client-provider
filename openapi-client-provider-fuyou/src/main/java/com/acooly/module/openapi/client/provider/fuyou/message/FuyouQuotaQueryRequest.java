package com.acooly.module.openapi.client.provider.fuyou.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouApiMsgInfo;
import com.acooly.module.openapi.client.provider.fuyou.domain.FuyouRequest;
import com.acooly.module.openapi.client.provider.fuyou.enums.FuyouServiceEnum;
import com.thoughtworks.xstream.annotations.XStreamAlias;

import org.hibernate.validator.constraints.NotBlank;

import lombok.Getter;
import lombok.Setter;

/**
 * @author zhike 2018/3/19 15:04
 */
@Getter
@Setter
@FuyouApiMsgInfo(service = FuyouServiceEnum.QUOTA_QUERY,type = ApiMessageType.Request)
@XStreamAlias("FM")
public class FuyouQuotaQueryRequest extends FuyouRequest{

    /**
     * 银行机构号
     * 支付的银行机构号
     */
    @XStreamAlias("INSCD")
    @NotBlank
    private String insCd;

    /**
     * 摘要数据
     */
    @XStreamAlias("SIGN")
    private String sign;

    @Override
    public String getSignStr() {
        return getPartner()+"|"+getInsCd()+"|";
    }
}
