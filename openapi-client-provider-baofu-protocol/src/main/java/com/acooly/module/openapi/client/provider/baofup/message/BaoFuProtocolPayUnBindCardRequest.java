package com.acooly.module.openapi.client.provider.baofup.message;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPApiMsgInfo;
import com.acooly.module.openapi.client.provider.baofup.domain.BaoFuPRequest;
import com.acooly.module.openapi.client.provider.baofup.enums.BaoFuPServiceEnum;
import com.acooly.module.openapi.client.provider.baofup.support.BaoFuPAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;

/**
 * @author zhike 2018/4/13 14:29
 */
@Getter
@Setter
@BaoFuPApiMsgInfo(service = BaoFuPServiceEnum.PROTOCOL_PAY_UNBIND_CARD,type = ApiMessageType.Request)
public class BaoFuProtocolPayUnBindCardRequest extends BaoFuPRequest {

    /**
     * 用户ID
     * 用户在商户平台唯一ID
     */
    @NotBlank
    @Size(max = 50)
    @BaoFuPAlias(value = "user_id")
    private String userId;

    /**
     * 签约协议号
     * 只有成功时该字段才有值
     * 加密方式：Base64转码后，使用数字信封指定的方式和密钥加密
     */
    @NotBlank
    @Size(max = 126)
    @BaoFuPAlias(value = "protocol_no",isEncrypt = true)
    private String protocolNo;

}
