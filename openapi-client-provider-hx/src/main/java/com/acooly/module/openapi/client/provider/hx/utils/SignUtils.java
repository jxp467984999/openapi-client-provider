/**
 * Project Name:payment
 * File Name:SignUtils.java
 * Package Name:cn.swiftpass.utils.payment.sign
 * Date:2014-6-27下午3:22:33
 */

package com.acooly.module.openapi.client.provider.hx.utils;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import lombok.extern.slf4j.Slf4j;


/**
 * ClassName:SignUtils
 * Function: 签名用的工具箱
 * Date:     2014-6-27 下午3:22:33
 *
 * @author
 */
@Slf4j
public class SignUtils {

    //3des加密
    public static String encrypt3DES(String encryptString, String encryptKey, String iv) throws Exception {
        byte encryptedData[];
        IvParameterSpec zeroIv = new IvParameterSpec(iv.getBytes("UTF-8"));
        SecretKeySpec key = new SecretKeySpec(encryptKey.getBytes("UTF-8"), "DESede");
        Cipher cipher = Cipher.getInstance("DESede/CBC/PKCS5Padding");
        cipher.init(1, key, zeroIv);
        encryptedData = cipher.doFinal(encryptString.getBytes("UTF-8"));
        String  is3DesString= new String(org.apache.commons.codec.binary.Base64.encodeBase64(encryptedData));
        return is3DesString;
    }

}

