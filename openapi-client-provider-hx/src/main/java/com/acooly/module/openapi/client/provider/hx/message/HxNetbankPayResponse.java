package com.acooly.module.openapi.client.provider.hx.message;


import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.hx.domain.HxApiMsgInfo;
import com.acooly.module.openapi.client.provider.hx.domain.HxResponse;
import com.acooly.module.openapi.client.provider.hx.enums.HxServiceEnum;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@HxApiMsgInfo(service = HxServiceEnum.hxNetbankPay, type = ApiMessageType.Response)
public class HxNetbankPayResponse extends HxResponse {

    /**
     * 网银HTML的from表单
     */
    private String netBankHtml;

}
