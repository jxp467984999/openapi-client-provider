/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-11-15 13:42 创建
 */
package com.acooly.module.openapi.client.provider.webank.partner.impl;

import com.acooly.module.openapi.client.provider.webank.WeBankProperties;
import com.acooly.module.openapi.client.provider.webank.partner.WeBankKeyPairManager;
import com.acooly.module.safety.support.CodecEnum;
import com.acooly.module.safety.support.KeyPair;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

/**
 * 此为获取partnerId的空实现 （demo）
 *
 * @author zhangpu 2017-11-15 13:42
 */
@Slf4j
@Service("weBankGetKeyPairManager")
public class WeBankKeyPairLoader implements WeBankKeyPairManager {

    @Autowired
    private WeBankProperties weBankProperties;

    private KeyPair keyPair;

    @Override
    public KeyPair getKeyPair(String partnerId) {

        //1.使用配置文件
        if (keyPair == null) {
            synchronized (this) {
                if (keyPair == null) {
                    keyPair = new KeyPair(weBankProperties.getPublicKey(),
                            weBankProperties.getPrivateKey());
                    keyPair.loadKeys();
                }
            }
        }
        keyPair.setSignatureCodec(CodecEnum.HEX);
        keyPair.setSignatureAlgo("SHA256withRSA");

        //2.使用商户号查询数据库里的公私钥

        return keyPair;
    }
}
