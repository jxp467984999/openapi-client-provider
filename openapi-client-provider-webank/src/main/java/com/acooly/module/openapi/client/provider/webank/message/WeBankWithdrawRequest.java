package com.acooly.module.openapi.client.provider.webank.message;

import com.acooly.module.openapi.client.api.anotation.ApiItem;
import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.webank.domain.WeBankApiMsgInfo;
import com.acooly.module.openapi.client.provider.webank.domain.WeBankRequest;
import com.acooly.module.openapi.client.provider.webank.enums.WeBankServiceEnum;
import com.acooly.module.openapi.client.provider.webank.message.dto.WeBankWithdrawInfo;

import lombok.Getter;
import lombok.Setter;

/**
 * @author fufeng
 */
@Getter
@Setter
@WeBankApiMsgInfo(service = WeBankServiceEnum.WEBANK_WITHDRAW, type = ApiMessageType.Request)
public class WeBankWithdrawRequest extends WeBankRequest {

    /**
     * 快捷申请业务信息
     */
    @ApiItem(value = "content")
    private WeBankWithdrawInfo weBankWithdrawInfo;
}
