/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhike
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.allinpay.domain;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;

/** @author zhike */
@Getter
@Setter
public class AllinpayRequest  extends AllinpayApiMessage{

  /**
   * 请求报文头信息
   */
  @XStreamAlias("INFO")
  private AllinpayRequestInfo requestInfo;
}
