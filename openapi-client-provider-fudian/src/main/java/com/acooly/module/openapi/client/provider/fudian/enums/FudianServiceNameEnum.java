/*
 * www.acooly.cn Inc.
 * Copyright (c) 2017 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2017-09-24 19:29 创建
 */
package com.acooly.module.openapi.client.provider.fudian.enums;

import com.acooly.core.utils.enums.Messageable;
import com.google.common.collect.Maps;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * BOSC服务名称枚举
 *
 * @author zhangpu 2017-09-24 19:29
 */
public enum FudianServiceNameEnum implements Messageable {

    UNKNOWN("UNKNOWN", "未知", ApiServiceType.SYNC),

    MIGRATION_DATA("/migration/dataMigration","数据迁移",ApiServiceType.ASYNC),

    QUERY_DOWNLOADLOGFILES("/query/downloadLogFiles","对账文件下载",ApiServiceType.SYNC),

    /**
     * 基础
     */
    USER_REGISTER("/user/register", "用户开户", ApiServiceType.REDIRECT),

    USER_CARD_BIND("/user/card/bind", "绑定银行卡", ApiServiceType.REDIRECT),

    USER_CARD_CANCELBIND("/user/card/cancelBind", "解绑银行卡(不需要审核)", ApiServiceType.REDIRECT),

    USER_CARD_AUDIT_CANCELBIND("/user/card/cancelCardBind", "解绑银行卡(需要审核)", ApiServiceType.REDIRECT),

    PHONE_UPDATE("/phone/update", "修改手机号码", ApiServiceType.REDIRECT),

    PWD_RESET("/pwd/reset", "重置交易密码", ApiServiceType.REDIRECT),

    BUSINESS_AUTHORIZATION("/business/authorization", "授权和取消授权", ApiServiceType.REDIRECT),

    CORP_REGISTER("/corp/registerNew", "企业开户", ApiServiceType.REDIRECT),

    CORP_MODIFY("/corp/modify", "企业信息变更", ApiServiceType.REDIRECT),


    /**
     * 资金
     */
    ACCOUNT_RECHARGE("/account/recharge", "PC充值", ApiServiceType.REDIRECT),
    APP_REALPAYRECHARGE("/app/realPayRecharge", "APP充值", ApiServiceType.REDIRECT),
    ACCOUNT_WITHDRAW("/account/withdraw", "提现", ApiServiceType.REDIRECT),
    MERCHANT_MERCHANTTRANSFER("/merchant/merchantTransfer", "商户转账(奖励)", ApiServiceType.SYNC),

    /**
     * 业务和交易
     */
    LOAN_CREATE("/loan/create", "发标", ApiServiceType.SYNC),
    LOAN_CANCEL("/loan/cancel", "流标", ApiServiceType.SYNC),
    LOAN_INVEST("/loan/invest", "手动投标", ApiServiceType.REDIRECT),
    LOAN_FASTINVEST("/loan/fastInvest", "自动投标", ApiServiceType.SYNC),
    LOAN_FULL("/loan/full", "满标", ApiServiceType.ASYNC),
    LOAN_REPAY("/loan/repay", "手动还款", ApiServiceType.REDIRECT),
    LOAN_FASTREPAY("/loan/fastRepay", "自动还款", ApiServiceType.ASYNC),
    LOAN_CALLBACK("/loan/callback", "投资人回款", ApiServiceType.SYNC),
    LOANCREDIT_INVEST("/loanCredit/invest", "债权认购", ApiServiceType.REDIRECT),

    /**
     * 查询
     */
    QUERY_TRADE("/query/trade", "订单查询", ApiServiceType.SYNC),
    QUERY_USER("/query/user", "用户查询", ApiServiceType.SYNC),
    QUERY_LOAN("/query/loan", "标的查询", ApiServiceType.SYNC),
    QUERY_LOGLOANACCOUNT("/query/logLoanAccount", "标的流水查询", ApiServiceType.SYNC),
    QUERY_LOGACCOUNT("/query/logAccount", "账户流水查询", ApiServiceType.SYNC),
    QUERY_RETREMIT("/query/retRemit", "提现退汇查询", ApiServiceType.SYNC);

    private final String code;
    private final String message;
    private final ApiServiceType apiServiceType;

    FudianServiceNameEnum(String code, String message, ApiServiceType apiServiceType) {
        this.code = code;
        this.message = message;
        this.apiServiceType = apiServiceType;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    @Override
    public String code() {
        return code;
    }

    @Override
    public String message() {
        return message;
    }

    public ApiServiceType getApiServiceType() {
        return apiServiceType;
    }

    public static Map<String, String> mapping() {
        Map<String, String> map = Maps.newLinkedHashMap();
        for (FudianServiceNameEnum type : values()) {
            map.put(type.getCode(), type.getMessage());
        }
        return map;
    }

    /**
     * 通过枚举值码查找枚举值。
     *
     * @param code 查找枚举值的枚举值码。
     * @return 枚举值码对应的枚举值。
     * @throws IllegalArgumentException 如果 code 没有对应的 Status 。
     */
    public static FudianServiceNameEnum find(String code) {
        for (FudianServiceNameEnum status : values()) {
            if (status.getCode().equals(code)) {
                return status;
            }
        }
        throw new IllegalArgumentException("BoscServiceNameEnum not legal:" + code);
    }

    /**
     * 获取全部枚举值。
     *
     * @return 全部枚举值。
     */
    public static List<FudianServiceNameEnum> getAll() {
        List<FudianServiceNameEnum> list = new ArrayList<FudianServiceNameEnum>();
        for (FudianServiceNameEnum status : values()) {
            list.add(status);
        }
        return list;
    }

    /**
     * 获取全部枚举值码。
     *
     * @return 全部枚举值码。
     */
    public static List<String> getAllCode() {
        List<String> list = new ArrayList<String>();
        for (FudianServiceNameEnum status : values()) {
            list.add(status.code());
        }
        return list;
    }

}
