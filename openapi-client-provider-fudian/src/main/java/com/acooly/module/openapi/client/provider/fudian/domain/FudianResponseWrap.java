/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-01-23 16:50 创建
 */
package com.acooly.module.openapi.client.provider.fudian.domain;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author zhangpu 2018-01-23 16:50
 */
@Getter
@Setter
public class FudianResponseWrap {

    /**
     * 接入商户标志（商户号:merchantNo）
     */
    @NotEmpty
    @Length(max = 8)
    private String merchantNo;

    @NotEmpty
    @Length(min = 2090, max = 2090)
    private String certInfo;

    @NotEmpty
    @Length(min = 512, max = 512)
    private String sign;

    private String retCode;

    private String retMsg;

}
