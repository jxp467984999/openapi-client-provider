/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-02-22 04:14:52 创建
 */package com.acooly.module.openapi.client.provider.fudian.message.fund;

import com.acooly.module.openapi.client.api.enums.ApiMessageType;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianApiMsg;
import com.acooly.module.openapi.client.provider.fudian.domain.FudianNotify;
import com.acooly.module.openapi.client.provider.fudian.enums.FudianServiceNameEnum;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

/**
 * @author zhangpu 2018-02-22 04:14:52
 */
@Getter
@Setter
@FudianApiMsg(service = FudianServiceNameEnum.LOANCREDIT_INVEST ,type = ApiMessageType.Notify)
public class LoancreditInvestNotify extends FudianNotify {

    /**
     * 承接金额
     * 投资的金额,以元为单位
     */
    @NotEmpty
    @Length(max=20)
    private String amount;

    /**
     * 承接人账户号
     * 承接人用户在本系统的唯一账户编号，由本系统生成.
     */
    @NotEmpty
    @Length(max=50)
    private String accountNo;

    /**
     * 承接人用户名
     * 承接人用户在本系统的唯一标识，由本系统生成
     */
    @NotEmpty
    @Length(max=32)
    private String userName;

    /**
     * 认购本金
     * 承接人购买的转让本金。注意，累计购买的转让本金不能大于发布的债权本金
     */
    @NotEmpty
    @Length(max=20)
    private String creditAmount;

    /**
     * 转让手续费
     * 债权转让过程中产生的手续费，支付给平台商户
     */
    @NotEmpty
    @Length(max=20)
    private String creditFee;

    /**
     * 转让手续费方式
     * 手续费到底由谁支出?1:转让人出2：承接人出
     */
    @NotEmpty
    @Length(min = 1,max=1)
    private String creditFeeType;

    /**
     * 债权挂牌ID
     * 平台发布债权标的唯一标识符
     */
    @NotEmpty
    @Length(max=50)
    private String creditNo;

    /**
     * 债权挂牌金额
     * 平台发布债权转让的金额
     */
    @NotEmpty
    @Length(max=20)
    private String creditNoAmount;

    /**
     * 原投资记录的订单日期
     * 原投资的订单日期yyyyMMdd
     */
    @NotEmpty
    @Length(min = 8,max=8)
    private String investOrderDate;

    /**
     * 原投资记录的订单号
     * 原投资的订单号，必须保证唯一性
     */
    @NotEmpty
    @Length(min = 20,max=20)
    private String investOrderNo;

    /**
     * 标的号
     * 标的号，由存管系统生成并确保唯一性.
     */
    @NotEmpty
    @Length(max=32)
    private String loanTxNo;

    /**
     * 商户号
     * 用于校验主体参数和业务参数一致性，保证参数的安全传输
     */
    @NotEmpty
    @Length(max=8)
    private String merchantNo;

    /**
     * 最原始投资记录订单日期
     * 最原始投资的订单日期yyyyMMdd
     */
    @NotEmpty
    @Length(min = 8,max=8)
    private String oriOrderDate;

    /**
     * 最原始投资记录订单号
     * 最原始投资的订单号，必须保证唯一性
     */
    @NotEmpty
    @Length(min = 20,max=20)
    private String oriOrderNo;

    /**
     * 已还款金额
     * 原投资已经还款的本金,默认：0.00
     */
    @NotEmpty
    @Length(max=20)
    private String repayedAmount;
}