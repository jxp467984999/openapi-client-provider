/*
 * www.acooly.cn Inc.
 * Copyright (c) 2018 All Rights Reserved
 */

/*
 * 修订记录:
 * zhangpu@acooly.cn 2018-01-23 16:50 创建
 */
package com.acooly.module.openapi.client.provider.yipay.domain;

import com.alibaba.fastjson.annotation.JSONField;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;

/**
 * @author zhangpu 2018-01-23 16:50
 */
@Getter
@Setter
public class YipayRequestWrap {

    /**
     * 接入商户标志（商户号:merchantNo）
     */
    @NotEmpty
    @Length(max = 8)
    private String merchantNo;

    @NotEmpty
    @Length(min = 2090, max = 2090)
    private String certInfo;

    @NotEmpty
    @Length(min = 512, max = 512)
    private String sign;

    @NotNull
    @JSONField(name = "data")
    private YipayMessage yipayMessage;


    public YipayRequestWrap() {
    }

    public YipayRequestWrap(String merchantNo, String certInfo, String sign, YipayMessage yipayMessage) {
        this.merchantNo = merchantNo;
        this.certInfo = certInfo;
        this.sign = sign;
        this.yipayMessage = yipayMessage;
    }
}
