package com.acooly.module.openapi.client.provider.jyt.message.dto;

import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;

/**
 * @author zhike 2018/6/19 22:05
 */
@Getter
@Setter
@XStreamAlias("body")
public class JytUnBindCardRequestBody implements Serializable {

    /**
     * 客户号
     * 持卡人在商户端客户号（一个身份证号对应一个客户号）
     */
    @Size(max = 30)
    @XStreamAlias("cust_no")
    @NotBlank
    private String userId;

    /**
     * 银行卡号
     */
    @Size(max = 30)
    @XStreamAlias("bank_card_no")
    @NotBlank
    private String bankCardNo;
}
