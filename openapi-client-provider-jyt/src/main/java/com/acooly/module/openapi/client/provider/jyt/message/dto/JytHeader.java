package com.acooly.module.openapi.client.provider.jyt.message.dto;

import com.acooly.core.utils.Dates;
import com.thoughtworks.xstream.annotations.XStreamAlias;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.Date;

/**
 * @author zhike 2018/5/7 23:56
 */
@Getter
@Setter
public class JytHeader implements Serializable {

    /** 版本号 标识本报文格式版本号。当前版本为：1.0.1 */
    @Size(max = 5)
    @XStreamAlias("version")
    private String version = "1.0.1";
    /** 报文类型 报文类型包括: 请求报文：01 响应报文：02 */
    @XStreamAlias("tran_type")
    private String tranType = "01";

    /** 商户编号 商户编号，由平台统一分配 */
    @Size(max = 12)
    @NotBlank
    @XStreamAlias("merchant_id")
    private String merchantId;

    /** 交易日期 客户端或服务端日期，格式：YYYYMMDD */
    @XStreamAlias("tran_date")
    @NotBlank
    private String tranDate = Dates.format(new Date(), "yyyyMMdd");

    /** 交易时间 客户端或服务端时间，格式：HHMMSS */
    @XStreamAlias("tran_time")
    @NotBlank
    private String tranTime = Dates.format(new Date(), "HHmmss");

    /** 交易流水号 交易流水号，唯一标识一笔交易，必须以商户号开头。格式建议为：商户号（12位）+ YYYYMMDDHHMMSS（14）+6位流水号(批量交易时，该流水号作为批次号) */
    @XStreamAlias("tran_flowid")
    private String tranFlowid;

    /** 交易代码 */
    @Size(max = 6)
    @NotBlank
    @XStreamAlias("tran_code")
    private String tranCode;
}
