package com.acooly.module.openapi.client.provider.bosc.message.response;

import lombok.Getter;
import lombok.Setter;

import java.util.Date;

import com.acooly.core.utils.Money;
import com.acooly.module.openapi.client.provider.bosc.domain.BoscResponseDomain;

@Getter
@Setter
public class OpenAcctResponse extends BoscResponseDomain {

	/**
	 * 证件号码
	 */
	private String idNo;

	/**
	 * 电子账户号
	 */
	private String eAcctNo;

	/**
	 * 电子户名
	 */
	private String eAcctName;

	/**
	 * 打款激活金额
	 */
	private Money amount;

	/**
	 * 打款激活最后日期
	 */
	private Date actiDeadline;

	/**
	 * 制单员号
	 */
	private String docuOpNo;

	/**
	 * 复核员号
	 */
	private String checkerNo;

}
