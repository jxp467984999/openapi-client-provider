/**
 * Project Name:payment
 * File Name:SignUtils.java
 * Package Name:cn.swiftpass.utils.payment.sign
 * Date:2014-6-27下午3:22:33
 */

package com.acooly.module.openapi.client.provider.wewallet.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;

import java.net.URLEncoder;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import lombok.extern.slf4j.Slf4j;

import static com.acooly.module.openapi.client.provider.wewallet.utils.StringHelper.byte2Hex;


/**
 * ClassName:SignUtils
 * Function: 签名用的工具箱
 * Date:     2014-6-27 下午3:22:33
 *
 * @author
 */
@Slf4j
public class SignUtils {

    public final static String RSA_CHIPER = "RSA/ECB/PKCS1Padding";

    /**
     * 编码
     */
    public final static String ENCODE = "UTF-8";

    /**
     * 1024bit 加密块 大小
     */
    public final static int ENCRYPT_KEYSIZE = 117;
    /**
     * 1024bit 解密块 大小
     */
    public final static int DECRYPT_KEYSIZE = 128;

    /**
     * 过滤参数
     *
     * @param sArray
     * @return
     * @author
     */
    public static Map<String, String> paraFilter(Map<String, String> sArray) {
        Map<String, String> result = new HashMap<String, String>(sArray.size());
        if (sArray == null || sArray.size() <= 0) {
            return result;
        }
        for (String key : sArray.keySet()) {
            String value = sArray.get(key);
            if (value == null || value.equals("") || key.equalsIgnoreCase("sign")) {
                continue;
            }
            result.put(key, value);
        }
        return result;
    }

    /**
     * <一句话功能简述>
     * <功能详细描述>将map转成String
     *
     * @param payParams
     * @return
     * @see [类、类#方法、类#成员]
     */
    public static String payParamsToString(Map<String, String> payParams) {
        return payParamsToString(payParams, false);
    }

    public static String payParamsToString(Map<String, String> payParams, boolean encoding) {
        return payParamsToString(new StringBuilder(), payParams, encoding);
    }

    /**
     * @param payParams
     * @return
     * @author
     */
    public static String payParamsToString(StringBuilder sb, Map<String, String> payParams, boolean encoding) {
        buildPayParams(sb, payParams, encoding);
        return sb.toString();
    }

    /**
     * @param payParams
     * @return
     * @author
     */
    public static void buildPayParams(StringBuilder sb, Map<String, String> payParams, boolean encoding) {
        List<String> keys = new ArrayList<String>(payParams.keySet());
        Collections.sort(keys);
        for (String key : keys) {
            sb.append(key).append("=");
            if (encoding) {
                sb.append(urlEncode(payParams.get(key)));
            } else {
                sb.append(payParams.get(key));
            }
            sb.append("&");
        }
        sb.setLength(sb.length() - 1);
    }

    /**
     * @param payParams
     * @return
     * @author
     */
    public static String encodeBizContent(String payParams) {
        LinkedHashMap<String, String> jsonMap = JSON.parseObject(payParams, new TypeReference<LinkedHashMap<String, String>>() {
        });
        JSONObject jsonObject = new JSONObject(true);
        for (Map.Entry<String, String> entry : jsonMap.entrySet()) {
            jsonObject.put(entry.getKey(),urlEncode(entry.getValue()));
        }
        return jsonObject.toJSONString();
    }

    public static String urlEncode(String str) {
        try {
            return URLEncoder.encode(str, "UTF-8");
        } catch (Throwable e) {
            return str;
        }
    }

    /**
     *
     * @param sortedParams
     * @return
     */
    public static String getReqContent(Map<String, String> sortedParams,String gatewayUrl) {
        StringBuffer content = new StringBuffer(512);
        content.append(gatewayUrl);
        List<String> keys = new ArrayList<String>(sortedParams.keySet());
        java.util. Collections.sort(keys);
        int index = 0;
        for (int i = 0; i < keys.size(); i++) {
            String key = keys.get(i);
            String value = sortedParams.get(key);
            content.append((index == 0 ? "" : "&") + key + "=" + value);
            index++;
        }

        return content.toString();
    }



    public static String doSign(List<String> values, String signTicket) {
        if (values ==  null) {
            throw new NullPointerException( "values is null");
        }
        values.removeAll(Collections.singleton( null));// remove null
        values.add(signTicket);
        java.util. Collections.sort(values);
        StringBuilder sb =  new StringBuilder();
        for (String s : values) {
            sb.append(s);
        }
        try {
            MessageDigest md = MessageDigest. getInstance ("sha1");
            md.update(sb.toString().getBytes("UTF-8"));
            String sign =  byte2Hex(md.digest());
            return sign;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }




}

