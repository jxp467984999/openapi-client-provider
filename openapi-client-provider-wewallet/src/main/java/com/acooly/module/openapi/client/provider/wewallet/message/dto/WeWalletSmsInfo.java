package com.acooly.module.openapi.client.provider.wewallet.message.dto;

import org.hibernate.validator.constraints.NotBlank;

import java.io.Serializable;

import lombok.Getter;
import lombok.Setter;

/**
 * 签约订单参数
 * @author fufeng
 */
@Getter
@Setter
public class WeWalletSmsInfo implements Serializable {

    /**
     * 商户号
     */
    @NotBlank
    protected String merId;

    /**
     * 交易订单号
     */
    @NotBlank
    protected String orderId;

    /**
     * 交易类型，固定值11
     */
    @NotBlank
    protected String bizType = "11";

    /**
     * 手机号
     */
    @NotBlank
    protected String phoneNo;

    /**
     * 业务名
     */
    @NotBlank
    protected String businessName;

}
