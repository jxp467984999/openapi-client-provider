/*
 * acooly.cn Inc.
 * Copyright (c) 2016 All Rights Reserved.
 * create by zhangpu 
 * date:2016年3月31日
 *
 */
package com.acooly.module.openapi.client.provider.fuiou.domain;

import com.acooly.core.utils.Ids;
import com.acooly.core.utils.ToString;
import com.acooly.module.openapi.client.api.message.ApiMessage;
import com.acooly.module.openapi.client.provider.fuiou.support.FuiouAlias;
import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * @author zhangpu
 */
public class FuiouApiMessage implements ApiMessage {

    private String service;

    @XStreamAlias("mchnt_cd")
    @FuiouAlias("mchnt_cd")
    private String partner;

    @XStreamAlias("mchnt_txn_ssn")
    @FuiouAlias("mchnt_txn_ssn")
    private String requestNo = Ids.oid();

    @FuiouAlias(value = "signature", sign = false)
    private String signature;

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getPartner() {
        return partner;
    }

    public void setPartner(String partner) {
        this.partner = partner;
    }

    public String getRequestNo() {
        return requestNo;
    }

    public void setRequestNo(String requestNo) {
        this.requestNo = requestNo;
    }

    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
