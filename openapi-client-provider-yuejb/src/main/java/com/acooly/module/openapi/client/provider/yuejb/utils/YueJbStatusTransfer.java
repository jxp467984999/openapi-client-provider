package com.acooly.module.openapi.client.provider.yuejb.utils;

import com.acooly.core.utils.enums.ResultStatus;

/**
 * 月结宝结果状态转换
 * Created by ouwen@yiji.com} on 2017/11/8.
 */
public class YueJbStatusTransfer {
    public static ResultStatus convert(String status){
        if("SUCCESS".equalsIgnoreCase(status)){
            return ResultStatus.success;
        }else if("FAIL".equalsIgnoreCase(status)){
            return ResultStatus.failure;
        }else {
            return ResultStatus.processing;
        }
    }
}
